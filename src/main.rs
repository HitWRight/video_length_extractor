use clap::{App, Arg};
use gstreamer::prelude::*;
use lettre::smtp::authentication::{Credentials, Mechanism};
use lettre::smtp::ConnectionReuseParameters;
use lettre::{
    ClientSecurity, ClientTlsParameters, EmailAddress, Envelope, SendableEmail, SmtpClient,
    Transport,
};
use log::{info, warn};
use native_tls::{Protocol, TlsConnector};
use pretty_env_logger;
use serde::{Deserialize, Serialize};
use std::os::windows::fs::symlink_file;

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Video {
    number: u64,
    length: u64,
}

struct IncorrectFile {
    number: u64,
    expected: u64,
    got: u64,
}

fn main() {
    println!("Initializing logging");
    pretty_env_logger::init();
    info!("Initializing gstreamer");
    gstreamer::init().expect("Failed to initialize gstreamer");

    let matches = App::new("Video length verifier")
        .version("0.1.0")
        .author("Ignas Lapėnas <ignas@lapenas.dev>")
        .about(
            "Takes video lengths from the database using MsSqlToJson.exe\n\
             (MsSqlToJson.dll.config file to configure the connection string to the database)\n\
             and compares the length using gstreamer library",
        )
        .arg(
            Arg::with_name("directories")
                .short("d")
                .long("directories")
                .multiple(true)
                .required(true)
                .min_values(1)
                .takes_value(true)
                .help("Sets the directories to scan for mp4 and avi files"),
        )
        .arg(
            Arg::with_name("send_to")
                .short("e")
                .long("send_to")
                .multiple(true)
                .required(true)
                .min_values(1)
                .takes_value(true)
                .help("Sets the emails to send the report to"),
        )
        .arg(
            Arg::with_name("server")
                .short("s")
                .long("server")
                .required(true)
                .takes_value(true)
                .help("Sets the smtp server address"),
        )
        .arg(
            Arg::with_name("username")
                .short("u")
                .long("username")
                .required(true)
                .takes_value(true)
                .help("Sets the username from which the email should be sent"),
        )
        .arg(
            Arg::with_name("password")
                .short("p")
                .long("password")
                .required(true)
                .takes_value(true)
                .help("Sets the email account password"),
        )
        .get_matches();

    let mut incorrect_files: Vec<IncorrectFile> = Vec::new();

    let data = get_data_from_db();
    for dir in matches.values_of("directories").unwrap() {
        info!("Scanning directory: {:?}", &dir);
        let files = std::fs::read_dir(dir).expect("Failed to read directory");

        for file in files {
            let path = file.unwrap().path();
            let newfile = std::path::Path::new("data");
            let newfile = newfile.join(std::path::Path::new(path.file_name().unwrap()));

            &format!("data/{}", path.file_name().unwrap().to_str().unwrap());

            symlink_file(path, newfile.clone()).expect("Failed to symlink");

            match get_file_length(&newfile) {
                Some(len) => {
                    for d in &data {
                        if newfile
                            .file_name()
                            .unwrap()
                            .to_str()
                            .unwrap()
                            .starts_with(&(d.number.to_string() + "."))
                            && d.length != len
                        {
                            incorrect_files.push(IncorrectFile {
                                number: d.number,
                                expected: d.length,
                                got: len,
                            });
                        }
                    }
                }
                None => (),
            };
            std::fs::remove_file(newfile).expect("Failed to remove file");
        }
    }

    let emails = matches
        .values_of("send_to")
        .unwrap()
        .map(|val| EmailAddress::new(val.to_string()).unwrap())
        .collect();

    let mut resulte: String = incorrect_files
        .into_iter()
        .map(|f| {
            format!(
                "Rastas neteisingas failas ({}) tikėtasi: {}s realus: {}s",
                f.number, f.expected, f.got
            )
        })
        .collect::<Vec<String>>()
        .join("\n");

    if resulte == "" {
        resulte = r#"
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  </head>
  <body>
    <p><font face="Consolas">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.--.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/`._&nbsp;`.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;/.&nbsp;.`&nbsp;\<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;)&nbsp;_&nbsp;(&nbsp;/<br>
&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;/.`-'&nbsp;\-.<br>
&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;)`.&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;\<br>
&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;.&nbsp;&nbsp;\^&nbsp;&nbsp;\&nbsp;|<br>
&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;|`.&nbsp;\`-|&nbsp;|<br>
&nbsp;&nbsp;&nbsp;&nbsp;//&nbsp;&nbsp;&nbsp;'&nbsp;\`\&nbsp;|<br>
&nbsp;&nbsp;&nbsp;.'&nbsp;&nbsp;&nbsp;&nbsp;.\&nbsp;.|&nbsp;|<br>
&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;``-._`.`-.<br>
&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`-._&nbsp;`.<br>
&nbsp;&nbsp;&nbsp;`.._.----`&nbsp;&nbsp;`.'<br>
&nbsp;_.--`&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_.-'<br>
'.__..--````&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visi&nbsp;klipai&nbsp;tvarkoje!<br>
</font>
    </p>
  </body>
</html>"#.to_string();
    }

    send_email(
        resulte,
        emails,
        matches.value_of("server").unwrap().to_string(),
        matches.value_of("username").unwrap().to_string(),
        matches.value_of("password").unwrap().to_string(),
    );

    info!("video_length_extractor.exe has finished.");
}

fn send_email(
    text: String,
    send_to: Vec<EmailAddress>,
    server: String,
    username: String,
    password: String,
) {
    let text = format!(
        "From: Aurora <{}> \n\
         To: {} \n\
         Subject: Rasti failai \n\
         Mime-Version: 1.0 \n\
         Content-Type: text/html \n\n\
         {}",
        username,
        send_to
            .clone()
            .into_iter()
            .map(|to| to.to_string())
            .collect::<Vec<String>>()
            .join(", "),
        text
    );
    let email = SendableEmail::new(
        Envelope::new(
            Some(EmailAddress::new("ignas@kata.lt".to_string()).unwrap()),
            send_to,
        )
        .unwrap(),
        "message_ids".to_string(),
        text.into_bytes(),
    );

    let mut tls_builder = TlsConnector::builder();
    tls_builder.min_protocol_version(Some(Protocol::Tlsv10));
    let tls_parameters = ClientTlsParameters::new(server.clone(), tls_builder.build().unwrap());
    let mut mailer = SmtpClient::new((&server[..], 465), ClientSecurity::Wrapper(tls_parameters))
        .unwrap()
        .authentication_mechanism(Mechanism::Login)
        .credentials(Credentials::new(username, password))
        .connection_reuse(ConnectionReuseParameters::ReuseUnlimited)
        .transport();

    mailer.send(email).unwrap();
}

fn get_data_from_db() -> Vec<Video> {
    info!("Retrieving video data from the database");
    let output = std::process::Command::new("ext/MsSqlToJson.exe")
        .output()
        .expect("MsSqlToJson failed to launch");

    serde_json::from_str(
        std::str::from_utf8(&output.stdout).expect("Failed to parse results to utf8"),
    )
    .expect("Failed to parse json")
}

fn get_file_length(file: &std::path::Path) -> Option<u64> {
    info!("Retrieving file metadata: {:?}", &file);
    let checkmp4 = std::ffi::OsStr::new("mp4");
    let checkavi = std::ffi::OsStr::new("avi");
    if file.to_str().unwrap().contains(" ") || file.to_str().unwrap().contains("_") {
        warn!("Ignoring unknown file: {:?}", &file);
        return None;
    }

    match file.extension() {
        None => {
            warn!("Ignoring file (No extension): {:?}", &file);
            return None;
        }
        Some(ext) if ext != checkmp4 && ext != checkavi =>{
            warn!("Ignoring file (Unknown extension): {:?}", &file);
            return None;
        },
        Some(_) => (),
    }

    let req = format!(
        "playbin uri=file:///{}/{}",
        std::env::current_dir()
            .unwrap()
            .to_str()
            .unwrap()
            .replace("\\", "/"),
        file.to_str().unwrap().replace("\\", "/")
    );
    println!("{}", &req);

    let pipeline = gstreamer::parse_launch(&req).expect("Failed to parse");
    pipeline
        .set_state(gstreamer::State::Playing)
        .expect("Failed to set state");
    let _result = pipeline.get_state(gstreamer::ClockTime(None));
    let duration: gstreamer::ClockTime =
        pipeline.query_duration().expect("failed to read duration");
    pipeline
        .set_state(gstreamer::State::Null)
        .expect("failed to set state to `Null`");

    info!("File metadata retrieved: {:?}", &file);
    Some(duration.seconds().expect("Failed to read duration"))
}
