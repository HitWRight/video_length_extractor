

# Aprašymas

Naudojant gstreamer gaunama klipo metadata (ilgis).  
Gautas ilgis palyginamas su tuo kuris yra suvestas sistemoje.  
Neatitikamus išsiunčiama atsakingiems asmenims.  
Jei visi klipai atitinka nustatytus ilgius, tuomet nusiunčiama erotika (ne visi feature request'ai nuobodus).  


# Reikalavimai

-   rustc kompiliatorius
-   cargo
-   gstreamer development bibliotekos


# Kompiliavimas

-   atsidaroma direktorija
-   paleidžiama komanda "cargo build"


# Instaliavimas

Nėra  


# Testavimas

Nėra  

